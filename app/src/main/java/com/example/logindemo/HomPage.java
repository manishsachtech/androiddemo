package com.example.logindemo;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class HomPage extends AppCompatActivity implements View.OnClickListener {
TextView home;
TextView sus;
TextView channel;
TextView profile;
FrameLayout frameLayout;
BottomNavigationView bottomNavigationView;

@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hom_page);
        home=findViewById(R.id.home);
        sus=findViewById(R.id.sus);
        channel=findViewById(R.id.chan);
        profile=findViewById(R.id.profile);
        frameLayout=findViewById(R.id.container);

        home.setOnClickListener(this);
        sus.setOnClickListener(this);
        channel.setOnClickListener(this);
        profile.setOnClickListener(this);

        addFragment(new HomeFragment());
    }

    private void replaceFragment(Fragment fragment){
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container,fragment);
        transaction.commit();
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container,fragment);
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();switch (id)
        {
            case R.id.home:
            {
                replaceFragment(new HomeFragment());
                break;
            }
            case R.id.sus:
            {
                replaceFragment(new ProfileFragment());
                break;
            }
            case R.id.chan:
            {
                replaceFragment(new HomeFragment());
                break;
            }
            case R.id.profile:
            {
                replaceFragment(new ProfileFragment());
                break;
            }
        }
    }
}
